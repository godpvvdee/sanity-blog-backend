// schemas/index.js
import blog from './banner'
import maral from './menu'
import author from './author'
import content from './content'
import post from './post'
import category from './category'
import contact from './contact'
export const schemaTypes = [blog,maral,author,content,post,category,contact]