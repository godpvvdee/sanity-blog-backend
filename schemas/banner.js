// end aldaa
export default  {
  name: 'banner',
  title: 'Нүүр',
  type: 'document',
  fields: [
    {
      name: 'title',
      title: 'Гарчиг',
      type: 'string',
    },
    {
      name: 'description',
      title: 'Тайлбар',
      type: 'text',
    },
    {
      name: 'buttonText',
      title: '  Товчны нэр',
      type: 'text',
    },
    {
      title: 'Poster',
      name: 'poster',
      type: 'image',
      options: {
        hotspot: true // <-- Defaults to false
      },
      
    }
  ],
}
