// schemas/pet.js
export default {
    name: 'tab',
    type: 'document',
    title: 'tab',
    fields: [
      {
        name: 'tab1',
        type: 'string',
        title: 'tab1'
      },
      {
        name: 'tab2',
        type: 'string',
        title: 'tab2'
      },
      {
        name: 'tab3',
        type: 'string',
        title: 'tab3'
      },
      {
        name: 'tab4',
        type: 'string',
        title: 'tab4'
      }
    ]
  }