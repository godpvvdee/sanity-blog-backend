import {defineCliConfig} from 'sanity/cli'

export default defineCliConfig({
  api: {
    projectId: 'mg0ajz0q',
    dataset: 'production'
  }
})
